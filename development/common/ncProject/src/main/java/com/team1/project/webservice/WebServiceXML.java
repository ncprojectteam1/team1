
package package_rezepov.webservice;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import package_rezepov.database.RequestHandler;
import package_rezepov.models.PowerEntity;
import package_rezepov.models.WindTurbineEntity;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.Collection;


/**
 * Created by eschy_000 on 29.03.2016.
 */

@Path("/xml")
public class WebServiceXML implements WebService {
    private static final Logger logger = LogManager.getLogger(WebServiceXML.class);

    @GET
    @Path("/getWindTurbines")
    @Produces(MediaType.APPLICATION_XML)
    public Collection<WindTurbineEntity> getWindTurbines() throws Exception {
        try {
            return RequestHandler.getWindTurbines();
        } catch (Exception e) {
            logger.error(e.getMessage());
            throw e;
        }
    }

    @GET
    @Path("/getWindTurbineById")
    @Produces("application/xml")
    public WindTurbineEntity getWindTurbineById(@QueryParam("id") String id) throws Exception {
        try {
            return RequestHandler.getWindTurbineById(id);
        } catch (Exception e) {
            logger.error(e.getMessage());
            throw e;
        }
    }

    @GET
    @Path("/getCustomerConsumption")
    @Produces("application/xml")
    public Collection<PowerEntity> getCustomerConsumption() throws Exception {
        try {
            return RequestHandler.getCustomerConsumption();
        } catch (Exception e) {
            logger.error(e.getMessage());
            throw e;
        }
    }

    @GET
    @Path("/getWindPowerPredictionById")
    @Produces("application/xml")
    public Collection<PowerEntity> getWindPowerPredictionById(@QueryParam("id") String id) throws Exception {
        try {
            return RequestHandler.getWindPowerPredictionById(id);
        } catch (Exception e) {
            logger.error(e.getMessage());
            throw e;
        }
    }

    @GET
    @Path("/getWindPowerPrediction")
    @Produces("application/xml")
    public Collection<PowerEntity> getWindPowerPrediction() throws Exception {
        try {
            return RequestHandler.getWindPowerPrediction();
        } catch (Exception e) {
            logger.error(e.getMessage());
            throw e;
        }
    }
}
