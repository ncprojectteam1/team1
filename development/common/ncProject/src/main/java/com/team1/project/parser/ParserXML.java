package package_rezepov.parser;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import package_rezepov.ApplicationContext;
import package_rezepov.Constants;
import package_rezepov.models.WeatherEntity;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.xpath.*;
import java.io.ByteArrayInputStream;
import java.text.DateFormat;
import java.text.DateFormatSymbols;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;

/**
 * Created by eschy_000 on 13.03.2016.
 */
class ParserXML implements Parser {
    private static final Logger logger = LogManager.getLogger(ParserXML.class);
    private ArrayList<WeatherEntity> parsedData;

    ParserXML() {
        parsedData  = new ArrayList<WeatherEntity>(36);
        for (int i = 0; i < 36; ++i) {
            parsedData.add(new WeatherEntity());
            parsedData.get(i).setCity(ApplicationContext.getInstance().getQ());
            parsedData.get(i).setCountry(ApplicationContext.getInstance().getCountry());
        }
    }

    public Collection<WeatherEntity> parse(String webData) throws Exception {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();

        SimpleDateFormat formatFrom = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");

        try {
            DocumentBuilder documentBuilder = factory.newDocumentBuilder();
            Document doc = documentBuilder.parse(new ByteArrayInputStream(webData.getBytes("UTF-8")));
            XPath xPath = XPathFactory.newInstance().newXPath();

            NodeList dateNodes = (NodeList) xPath.compile(Constants.TIME_PATH).evaluate(doc, XPathConstants.NODESET);
            xPath.reset();
            NodeList windNodes = (NodeList) xPath.compile(Constants.WIND_PATH).evaluate(doc, XPathConstants.NODESET);
            xPath.reset();
            NodeList humidityNodes = (NodeList) xPath.compile(Constants.HUMIDITY_PATH).evaluate(
                    doc, XPathConstants.NODESET);
            xPath.reset();
            NodeList temperatureNodes = (NodeList) xPath.compile(Constants.TEMPERATURE_PATH).evaluate(
                    doc, XPathConstants.NODESET);
            xPath.reset();
            NodeList pressureNodes = (NodeList) xPath.compile(Constants.PRESSURE_PATH).evaluate(
                    doc, XPathConstants.NODESET);

            for (int i = 0; i < 36; ++i) {
                parsedData.get(i).setDate(formatFrom.parse(dateNodes.item(i).getNodeValue()));
                parsedData.get(i).setWind(Double.parseDouble(windNodes.item(i).getNodeValue()));
                parsedData.get(i).setHumidity(Long.parseLong(humidityNodes.item(i).getNodeValue()));
                parsedData.get(i).setTemperature(Double.parseDouble(temperatureNodes.item(i).getNodeValue()));
                parsedData.get(i).setPressure(Double.parseDouble(pressureNodes.item(i).getNodeValue()));
            }

            return parsedData;
        } catch (Exception e) {
            logger.error(e.getMessage());
            throw e;
        }
    }
}
