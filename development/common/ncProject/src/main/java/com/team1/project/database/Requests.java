package package_rezepov.database;

/**
 * Created by eschy_000 on 21.03.2016.
 */
public class Requests {
    static final String INSERT_NEW_CITY = "INSERT INTO locations (city_name, country_name) " +
            "VALUES (?, ?) " +
            "ON DUPLICATE KEY UPDATE city_name = ?";
    static final String GET_LOCATION_ID = "SELECT location_id " +
            "FROM locations " +
            "WHERE city_name = ? " +
            "AND country_name = ?";
    static final String UPDATE_WEATHER = "INSERT INTO forecasts " +
            "(city_id, pressure, temperature, humidity, wind, date) " +
            "VALUES (?, ?, ?, ?, ?, ?) " +
            "ON DUPLICATE KEY UPDATE pressure = ?, temperature = ?, humidity = ?, wind = ?";
    static final String GET_WIND_TURBINES = "SELECT generator_id, " +
            "cut_in, cut_nom, cut_out, power_nom, number_of_blades " +
            "FROM generators " +
            "LEFT JOIN models " +
            "ON generators.model_id = models.model_id";
    static final String GET_WIND_TURBINE_BY_ID = "SELECT cut_in, " +
            "cut_nom, cut_out, power_nom, number_of_blades " +
            "FROM generators " +
            "LEFT JOIN models " +
            "ON generators.model_id = models.model_id " +
            "WHERE generator_id = ?";
    static final String GET_CUSTOMER_CONSUMPTION = "SELECT power_value, time " +
            "FROM consumption";
    static final String GET_ALL_WEATHER = "SELECT city_name, " +
            "country_name, pressure, temperature, humidity, wind, date " +
            "FROM forecasts " +
            "LEFT JOIN locations " +
            "ON forecasts.location_id = locations.location_id";

}
