package package_rezepov;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * Created by eschy_000 on 13.03.2016.
 */
public class ApplicationContext {
    private static ApplicationContext instance;
    private static final Logger logger = LogManager.getLogger(ApplicationContext.class);

    private String httpUrl;
    private String q;
    private String country;
    private String mode;
    private String appid;

    private String dbUrl;
    private String user;
    private String password;

    public String getHttpUrl() {
        return httpUrl;
    }

    public String getQ() {
        return q;
    }

    public String getCountry() {
        return country;
    }

    public String getMode() {
        return mode;
    }

    public String getAppid() {
        return appid;
    }

    public String getDbUrl() {
        return dbUrl;
    }

    public String getUser() {
        return user;
    }

    public String getPassword() {
        return password;
    }

    private ApplicationContext() {
        InputStream is;
        Properties property = new Properties();

        try {
            is = getClass().getClassLoader().getResourceAsStream("config.properties");

            property.load(is);

            httpUrl = property.getProperty(Constants.HTTP_URL);
            q = property.getProperty(Constants.HTTP_CITY);
            country = property.getProperty(Constants.HTTP_COUNTRY);
            mode = property.getProperty(Constants.HTTP_MODE);
            appid = property.getProperty(Constants.HTTP_APPID);

            dbUrl = property.getProperty(Constants.DB_URL);
            user = property.getProperty(Constants.DB_USER);
            password = property.getProperty(Constants.DB_PASSWORD);

            if (logger.isInfoEnabled()) {
                logger.info("Property file has been read successfully.");
            }
        } catch (IOException e) {
            logger.error("Cannot read property file.");
        }
    }

    public static synchronized ApplicationContext getInstance() {
        if (instance == null) {
            instance = new ApplicationContext();
        }
        return instance;
    }
}
