package package_rezepov.parser;

import package_rezepov.models.WeatherEntity;

import java.util.Collection;

/**
 * Created by eschy_000 on 13.03.2016.
 */
public interface Parser {
    Collection<WeatherEntity> parse(String webData) throws Exception;
}
