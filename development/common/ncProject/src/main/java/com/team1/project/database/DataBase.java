package package_rezepov.database;

import java.sql.Connection;
import java.sql.ResultSet;

/**
 * Created by eschy_000 on 13.03.2016.
 */
public interface DataBase {
    void updateWeather() throws Exception;
    Connection openConnection() throws Exception;
    void closeConnection(Connection con);
    ResultSet selectData(Connection con, String sql) throws Exception;
}
