package package_rezepov.httpurlconnection;

/**
 * Created by eschy_000 on 13.03.2016.
 */
public interface HttpUrlConnection {
    String sendGet() throws Exception;

    String sendPost();
}
