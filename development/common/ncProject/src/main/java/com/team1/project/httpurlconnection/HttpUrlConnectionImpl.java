package package_rezepov.httpurlconnection;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import package_rezepov.ApplicationContext;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by eschy_000 on 13.03.2016.
 */
public class HttpUrlConnectionImpl implements HttpUrlConnection {
    private static final Logger logger = LogManager.getLogger(HttpUrlConnectionImpl.class);

    public String sendGet() throws Exception {
        ApplicationContext cfg = ApplicationContext.getInstance();
        String url = cfg.getHttpUrl() + "?q=" + cfg.getQ() + "," +cfg.getCountry() +
                "&mode=" + cfg.getMode() + "&appid=" + cfg.getAppid();

        try {
            URL obj = new URL(url);
            HttpURLConnection con = (HttpURLConnection) obj.openConnection();

            con.setRequestMethod("GET");

            int responseCode = con.getResponseCode();
            if (logger.isInfoEnabled()) {
                logger.info("\nSending 'GET' request to URL : {}\nResponse Code : {}", url, responseCode);
            }

            BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
            String inputLine;
            StringBuilder response = new StringBuilder();

            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();

            return response.toString();
        } catch (Exception e) {
            logger.error(e.getMessage());
            throw e;
        }
    }

    public String sendPost() {
        return null;
    }
}
