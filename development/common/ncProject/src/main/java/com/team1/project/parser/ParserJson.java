package package_rezepov.parser;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import package_rezepov.ApplicationContext;
import package_rezepov.Constants;
import package_rezepov.models.WeatherEntity;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;


/**
 * Created by eschy_000 on 13.03.2016.
 */
class ParserJson implements Parser {
    private static final Logger logger = LogManager.getLogger(ParserJson.class);
    private ArrayList<WeatherEntity> parsedData;

    ParserJson() {
        parsedData = new ArrayList<WeatherEntity>(36);
        for (int i = 0; i < 36; ++i) {
            parsedData.set(i, new WeatherEntity());
            parsedData.get(i).setCity(ApplicationContext.getInstance().getQ());
            parsedData.get(i).setCountry(ApplicationContext.getInstance().getCountry());
        }
    }

    public Collection<WeatherEntity> parse(String webData) throws Exception {
        try {
            byte[] weatherData = webData.getBytes("UTF-8");

            ObjectMapper objectMapper = new ObjectMapper();
            JsonNode listNode = objectMapper.readTree(weatherData).path("list");

            List<JsonNode> dateNodes = listNode.findValues(Constants.TIME_NODE);
            List<JsonNode> windNodes = listNode.findValues(Constants.WIND_NODE);
            List<JsonNode> humidityNodes = listNode.findValues(Constants.HUMIDITY_NODE);
            List<JsonNode> temperatureNodes = listNode.findValues(Constants.TEMPERATURE_NODE);
            List<JsonNode> pressureNodes = listNode.findValues(Constants.PRESSURE_NODE);

            for (int i = 0; i < 36; ++i) {
                parsedData.get(i).setDate(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(dateNodes.get(i).asText()));
                parsedData.get(i).setWind(windNodes.get(i).asDouble());
                parsedData.get(i).setHumidity(humidityNodes.get(i).asLong());
                parsedData.get(i).setTemperature(temperatureNodes.get(i).asDouble());
                parsedData.get(i).setPressure(pressureNodes.get(i).asDouble());
            }

            return parsedData;
        } catch (Exception e) {
            logger.error(e.getMessage());
            throw e;
        }
    }
}