package package_rezepov.models;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created by eschy_000 on 18.03.2016.
 */

@XmlRootElement
public class WindTurbineEntity extends Entity{
    private Long id;
    private Double cutIn;
    private Double cutNom;
    private Double cutOut;
    private Integer powerNom;
    private Long numberOfBlades;

    @XmlElement
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @XmlElement
    public Double getCutIn() {
        return this.cutIn;
    }

    public void setCutIn(Double cutIn) {
        this.cutIn = cutIn;
    }

    @XmlElement
    public Double getCutNom() {
        return this.cutNom;
    }

    public void setCutNom(Double cutNom) {
        this.cutNom = cutNom;
    }

    @XmlElement
    public Double getCutOut() {
        return this.cutOut;
    }

    public void setCutOut(Double cutOut) {
        this.cutOut = cutOut;
    }

    @XmlElement
    public Integer getPowerNom() {
        return this.powerNom;
    }

    public void setPowerNom(Integer powerNom) {
        this.powerNom = powerNom;
    }

    @XmlElement
    public Long getNumberOfBlades() {
        return this.numberOfBlades;
    }

    public void setNumberOfBlades(Long numberOfBlades) {
        this.numberOfBlades = numberOfBlades;
    }
}
