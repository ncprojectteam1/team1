package package_rezepov.database;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import package_rezepov.Constants;
import package_rezepov.models.PowerEntity;
import package_rezepov.models.WeatherEntity;
import package_rezepov.models.WindTurbineEntity;
import package_rezepov.utils.PowerCalculation;

import javax.ws.rs.WebApplicationException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.Date;
import java.util.LinkedList;

/**
 * Created by eschy_000 on 29.03.2016.
 */
public class RequestHandler {
    private static final Logger logger = LogManager.getLogger(RequestHandler.class);

    public static Collection<WindTurbineEntity> getWindTurbines() throws Exception {
        LinkedList<WindTurbineEntity> collection = new LinkedList<WindTurbineEntity>();
        DataBase db = new DataBaseImpl();
        Connection con = null;

        try {
            con = db.openConnection();
            ResultSet resultSet = new DataBaseImpl().selectData(con, Requests.GET_WIND_TURBINES);

            resultSet.beforeFirst();
            while (resultSet.next()) {
                collection.addLast(new WindTurbineEntity());
                collection.getLast().setId(resultSet.getLong(Constants.GENERATOR_ID));
                collection.getLast().setCutIn(resultSet.getDouble(Constants.CUT_IN));
                collection.getLast().setCutNom(resultSet.getDouble(Constants.CUT_NOM));
                collection.getLast().setCutOut(resultSet.getDouble(Constants.CUT_OUT));
                collection.getLast().setPowerNom(resultSet.getInt(Constants.POWER_NOM));
                collection.getLast().setNumberOfBlades(resultSet.getLong(Constants.NUMBER_OF_BLADES));
            }
            return collection;
        } catch (ClassNotFoundException e) {
            logger.error(e.getMessage());
            throw new WebApplicationException(404);
        } catch (SQLException e) {
            logger.error(e.getMessage());
            throw e;
        } finally {
            db.closeConnection(con);
        }
    }

    public static WindTurbineEntity getWindTurbineById(String id) throws Exception {
        DataBase db = new DataBaseImpl();
        Connection con = null;

        try {
            con = db.openConnection();
            ResultSet resultSet = db.selectData(con, Requests.GET_WIND_TURBINE_BY_ID.replace("?", id));
            WindTurbineEntity entity = new WindTurbineEntity();

            resultSet.first();
            entity.setId(Long.parseLong(id));
            entity.setCutIn(resultSet.getDouble(Constants.CUT_IN));
            entity.setCutNom(resultSet.getDouble(Constants.CUT_NOM));
            entity.setCutOut(resultSet.getDouble(Constants.CUT_OUT));
            entity.setPowerNom(resultSet.getInt(Constants.POWER_NOM));
            entity.setNumberOfBlades(resultSet.getLong(Constants.NUMBER_OF_BLADES));

            return entity;
        } catch (ClassNotFoundException e) {
            logger.error(e.getMessage());
            throw new WebApplicationException(404);
        } catch (SQLException e) {
            logger.error(e.getMessage());
            throw new WebApplicationException(404);
        } finally {
            db.closeConnection(con);
        }
    }

    public static Collection<PowerEntity> getCustomerConsumption() throws Exception {
        LinkedList<PowerEntity> collection = new LinkedList<PowerEntity>();
        ResultSet resultSet;
        DataBase db = new DataBaseImpl();
        Connection con = null;

        try {
            con = db.openConnection();
            resultSet = db.selectData(con, Requests.GET_CUSTOMER_CONSUMPTION);
            resultSet.beforeFirst();
            while (resultSet.next()) {
                collection.addLast(new PowerEntity());
                collection.getLast().setDate(resultSet.getTimestamp(Constants.TIME));
                collection.getLast().setPower(resultSet.getInt(Constants.POWER_VALUE));
            }
        } catch (ClassNotFoundException e) {
            logger.error(e.getMessage());
            throw new WebApplicationException(404);
        } catch (SQLException e) {
            logger.error(e.getMessage());
            throw new WebApplicationException(404);
        } finally {
            db.closeConnection(con);
        }

        return collection;
    }

    public static Collection<PowerEntity> getWindPowerPredictionById(String id) throws Exception{
        DataBase db = new DataBaseImpl();
        Connection con = null;
        ResultSet resultSet;
        LinkedList<WeatherEntity> collection = new LinkedList<WeatherEntity>();

        try {
            con = db.openConnection();

            resultSet = db.selectData(con, Requests.GET_ALL_WEATHER);
            resultSet.beforeFirst();
            while (resultSet.next()) {
                collection.addLast(new WeatherEntity());
                collection.getLast().setCity(resultSet.getString(Constants.CITY));
                collection.getLast().setCountry(resultSet.getString(Constants.COUNTRY));
                collection.getLast().setPressure(resultSet.getDouble(Constants.PRESSURE));
                collection.getLast().setTemperature(resultSet.getDouble(Constants.TEMPERATURE));
                collection.getLast().setHumidity(resultSet.getLong(Constants.HUMIDITY));
                collection.getLast().setWind(resultSet.getDouble(Constants.WIND));
                collection.getLast().setDate(new Date(resultSet.getTimestamp(Constants.DATE).getTime()));
            }
            return new PowerCalculation(getWindTurbineById(id)).estimatePower(collection);
        } catch (ClassNotFoundException e) {
            logger.error(e.getMessage());
            throw new WebApplicationException(404);
        } catch (SQLException e) {
            logger.error(e.getMessage());
            throw new WebApplicationException(404);
        } finally {
            db.closeConnection(con);
        }
    }

    public static Collection<PowerEntity> getWindPowerPrediction() throws Exception {
        LinkedList<PowerEntity> collection = new LinkedList<PowerEntity>();
        Collection<WindTurbineEntity> turbines = getWindTurbines();

        for (WindTurbineEntity turbine: turbines) {
            collection.addAll(getWindPowerPredictionById(turbine.getId().toString()));
        }

        return collection;
    }
}
