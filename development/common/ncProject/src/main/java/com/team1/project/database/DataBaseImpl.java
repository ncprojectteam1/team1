package package_rezepov.database;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import package_rezepov.ApplicationContext;
import package_rezepov.models.WeatherEntity;
import package_rezepov.httpurlconnection.HttpUrlConnectionImpl;
import package_rezepov.parser.ParserFactory;

import java.sql.*;
import java.text.SimpleDateFormat;
import java.util.Collection;

/**
 * Created by eschy_000 on 13.03.2016.
 */
public class DataBaseImpl implements DataBase {
    private static final Logger logger = LogManager.getLogger(DataBaseImpl.class);

    public void updateWeather() throws Exception {
        writeData(ParserFactory.getParser().parse(new HttpUrlConnectionImpl().sendGet()));
    }

    public ResultSet selectData(Connection con, String sql) throws Exception {
        Statement statement;

        try{
            statement = con.createStatement();
            return statement.executeQuery(sql);
        } catch (SQLException e) {
            logger.error(e.getMessage());
            throw e;
        }
    }

    public Connection openConnection() throws ClassNotFoundException, SQLException {
        Class.forName("com.mysql.jdbc.Driver");

        ApplicationContext cfg = ApplicationContext.getInstance();
        String url = cfg.getDbUrl();
        String name = cfg.getUser();
        String password = cfg.getPassword();

        Connection con = DriverManager.getConnection(url, name, password);
        if (logger.isInfoEnabled()) {
            logger.info("Connected.");
        }

        return con;
    }

    public void closeConnection(Connection con) {
        if (con != null) {
            try {
                con.close();
                if (logger.isInfoEnabled()) {
                    logger.info("Connection closed.");
                }
            } catch (SQLException e) {
                logger.error("Failed to close connection to database.");
            }
        }
    }

    private void writeData(Collection<WeatherEntity> data) throws SQLException {
        Connection con = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet;
        SimpleDateFormat formatTo = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        int location_id;
        try {
            con = openConnection();
            con.setAutoCommit(false);

            preparedStatement = con.prepareStatement(Requests.INSERT_NEW_CITY);
            preparedStatement.setString(1, data.iterator().next().getCity());
            preparedStatement.setString(2, data.iterator().next().getCountry());
            preparedStatement.setString(3, data.iterator().next().getCity());
            preparedStatement.executeUpdate();
            con.commit();

            preparedStatement = con.prepareStatement(Requests.GET_LOCATION_ID);
            preparedStatement.setString(1, data.iterator().next().getCity());
            preparedStatement.setString(2, data.iterator().next().getCountry());
            resultSet = preparedStatement.executeQuery();
            resultSet.first();
            location_id = resultSet.getInt(1);

            preparedStatement = con.prepareStatement(Requests.UPDATE_WEATHER);
            for (WeatherEntity weather: data) {
                preparedStatement.setInt(1, location_id);
                preparedStatement.setDouble(2, weather.getPressure());
                preparedStatement.setDouble(3, weather.getTemperature());
                preparedStatement.setLong(4, weather.getHumidity());
                preparedStatement.setDouble(5, weather.getWind());
                preparedStatement.setString(6, formatTo.format(weather.getDate()));
                preparedStatement.setDouble(7, weather.getPressure());
                preparedStatement.setDouble(8, weather.getTemperature());
                preparedStatement.setLong(9, weather.getHumidity());
                preparedStatement.setDouble(10, weather.getWind());
                preparedStatement.addBatch();
            }
            preparedStatement.executeBatch();
            con.commit();
            if (logger.isInfoEnabled()) {
                logger.info("Successfully updated weather data.");
            }
        } catch (SQLException e) {
            logger.error("Failed to open or renew the database.");
        } catch (ClassNotFoundException e) {
            logger.error("Failed to open a connection.");
        } finally {
            try {
                if (preparedStatement != null) {
                    preparedStatement.close();
                }
            } catch (SQLException e) {
                logger.error(e.getMessage());
            }
            closeConnection(con);
        }
    }
}
