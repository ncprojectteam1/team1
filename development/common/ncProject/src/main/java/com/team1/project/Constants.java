package package_rezepov;

/**
 * Created by eschy_000 on 15.03.2016.
 */
public class Constants {
    public static final String HTTP_URL = "http.url";
    public static final String HTTP_CITY = "http.q";
    public static final String HTTP_COUNTRY = "http.country";
    public static final String HTTP_MODE = "http.mode";
    public static final String HTTP_APPID = "http.appid";

    public static final String DB_URL = "db.url";
    public static final String DB_USER = "db.user";
    public static final String DB_PASSWORD ="db.password";

    public static final String TIME_NODE = "dt_txt";
    public static final String WIND_NODE = "speed";
    public static final String HUMIDITY_NODE = "humidity";
    public static final String TEMPERATURE_NODE = "temperature";
    public static final String PRESSURE_NODE = "pressure";

    public static final String TIME_PATH = "//time/@to";
    public static final String WIND_PATH = "//windSpeed/@mps";
    public static final String HUMIDITY_PATH = "//humidity/@value";
    public static final String TEMPERATURE_PATH = "//temperature/@value";
    public static final String PRESSURE_PATH = "//pressure/@value";

    public static final String GENERATOR_ID = "generator_id";
    public static final String CUT_IN = "cut_in";
    public static final String CUT_NOM = "cut_nom";
    public static final String CUT_OUT = "cut_out";
    public static final String POWER_NOM = "power_nom";
    public static final String NUMBER_OF_BLADES = "number_of_blades";

    public static final String TIME = "time";
    public static final String POWER_VALUE = "power_value";

    public static final String CITY = "city_name";
    public static final String COUNTRY = "country_name";
    public static final String PRESSURE = "pressure";
    public static final String TEMPERATURE = "temperature";
    public static final String HUMIDITY = "humidity";
    public static final String WIND = "wind";
    public static final String DATE = "date";
}
