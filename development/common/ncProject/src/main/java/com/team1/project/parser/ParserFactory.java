package package_rezepov.parser;

import package_rezepov.ApplicationContext;

/**
 * Created by eschy_000 on 13.03.2016.
 */
public class ParserFactory {
    public static Parser getParser() throws IllegalStateException {
        ApplicationContext cfg = ApplicationContext.getInstance();
        if (cfg.getMode().equals("xml")) {
            return new ParserXML();
        } else if (cfg.getMode().equals("json")) {
            return new ParserJson();
        } else {
            throw new IllegalStateException("Failed to create parser for given format. " +
                    "Only xml and json are supported.");
        }
    }
}
