package package_rezepov.webservice;

import package_rezepov.models.PowerEntity;
import package_rezepov.models.WindTurbineEntity;

import java.sql.SQLException;
import java.util.Collection;

/**
 * Created by eschy_000 on 19.03.2016.
 */
public interface WebService {
    Collection<WindTurbineEntity> getWindTurbines() throws Exception;
    WindTurbineEntity getWindTurbineById(String id) throws Exception;
    Collection<PowerEntity>getCustomerConsumption() throws Exception;
    Collection<PowerEntity> getWindPowerPredictionById(String id) throws Exception;
    Collection<PowerEntity> getWindPowerPrediction() throws Exception;
}
