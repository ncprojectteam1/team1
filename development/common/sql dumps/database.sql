-- MySQL dump 10.13  Distrib 5.7.9, for Win64 (x86_64)
--
-- Host: localhost    Database: rezepov
-- ------------------------------------------------------
-- Server version	5.7.11-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `consumption`
--

DROP TABLE IF EXISTS `consumption`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `consumption` (
  `customer_id` int(11) NOT NULL,
  `time` datetime NOT NULL,
  `power_value` int(11) NOT NULL,
  PRIMARY KEY (`customer_id`,`time`),
  KEY `consumer_id_consumption_idx` (`customer_id`),
  CONSTRAINT `customer_id_consumption` FOREIGN KEY (`customer_id`) REFERENCES `customers` (`customer_id`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `consumption`
--

LOCK TABLES `consumption` WRITE;
/*!40000 ALTER TABLE `consumption` DISABLE KEYS */;
INSERT INTO `consumption` VALUES (1,'2016-01-01 00:00:00',50),(1,'2016-01-01 03:00:00',15),(1,'2016-01-01 06:00:00',0),(1,'2016-01-01 09:00:00',15),(1,'2016-01-01 12:00:00',50),(1,'2016-01-01 15:00:00',85),(1,'2016-01-01 18:00:00',100),(1,'2016-01-01 21:00:00',85),(1,'2016-01-02 00:00:00',50),(1,'2016-01-02 03:00:00',15),(1,'2016-01-02 06:00:00',0),(1,'2016-01-02 09:00:00',15),(1,'2016-01-02 12:00:00',50),(1,'2016-01-02 15:00:00',85),(1,'2016-01-02 18:00:00',100),(1,'2016-01-02 21:00:00',85),(1,'2016-01-03 00:00:00',50),(1,'2016-01-03 03:00:00',15),(1,'2016-01-03 06:00:00',0),(1,'2016-01-03 09:00:00',15),(1,'2016-01-03 12:00:00',50),(1,'2016-01-03 15:00:00',85),(1,'2016-01-03 18:00:00',100),(1,'2016-01-03 21:00:00',85),(1,'2016-01-04 00:00:00',50),(1,'2016-01-04 03:00:00',15),(1,'2016-01-04 06:00:00',0),(1,'2016-01-04 09:00:00',15),(1,'2016-01-04 12:00:00',50),(1,'2016-01-04 15:00:00',85),(1,'2016-01-04 18:00:00',100),(1,'2016-01-04 21:00:00',85),(1,'2016-01-05 00:00:00',50),(1,'2016-01-05 03:00:00',15),(1,'2016-01-05 06:00:00',0),(1,'2016-01-05 09:00:00',15),(1,'2016-01-05 12:00:00',50),(1,'2016-01-05 15:00:00',85),(1,'2016-01-05 18:00:00',100),(1,'2016-01-05 21:00:00',85),(1,'2016-01-06 00:00:00',50),(1,'2016-01-06 03:00:00',15),(1,'2016-01-06 06:00:00',0),(1,'2016-01-06 09:00:00',15),(1,'2016-01-06 12:00:00',50),(1,'2016-01-06 15:00:00',85),(1,'2016-01-06 18:00:00',100),(1,'2016-01-06 21:00:00',85),(1,'2016-01-07 00:00:00',50),(1,'2016-01-07 03:00:00',15),(1,'2016-01-07 06:00:00',0),(1,'2016-01-07 09:00:00',15),(1,'2016-01-07 12:00:00',50),(1,'2016-01-07 15:00:00',85),(1,'2016-01-07 18:00:00',100),(1,'2016-01-07 21:00:00',85),(1,'2016-01-08 00:00:00',50),(1,'2016-01-08 03:00:00',15),(1,'2016-01-08 06:00:00',0),(1,'2016-01-08 09:00:00',15),(1,'2016-01-08 12:00:00',50),(1,'2016-01-08 15:00:00',85),(1,'2016-01-08 18:00:00',100),(1,'2016-01-08 21:00:00',85),(1,'2016-01-09 00:00:00',50),(1,'2016-01-09 03:00:00',15),(1,'2016-01-09 06:00:00',0),(1,'2016-01-09 09:00:00',15),(1,'2016-01-09 12:00:00',50),(1,'2016-01-09 15:00:00',85),(1,'2016-01-09 18:00:00',100),(1,'2016-01-09 21:00:00',85),(1,'2016-01-10 00:00:00',50),(1,'2016-01-10 03:00:00',15),(1,'2016-01-10 06:00:00',0),(1,'2016-01-10 09:00:00',15),(1,'2016-01-10 12:00:00',50),(1,'2016-01-10 15:00:00',85),(1,'2016-01-10 18:00:00',100),(1,'2016-01-10 21:00:00',85),(1,'2016-01-11 00:00:00',50),(1,'2016-01-11 03:00:00',15),(1,'2016-01-11 06:00:00',0),(1,'2016-01-11 09:00:00',15),(1,'2016-01-11 12:00:00',50),(1,'2016-01-11 15:00:00',85),(1,'2016-01-11 18:00:00',100),(1,'2016-01-11 21:00:00',85),(1,'2016-01-12 00:00:00',50),(1,'2016-01-12 03:00:00',15),(1,'2016-01-12 06:00:00',0),(1,'2016-01-12 09:00:00',15),(1,'2016-01-12 12:00:00',50),(1,'2016-01-12 15:00:00',85),(1,'2016-01-12 18:00:00',100),(1,'2016-01-12 21:00:00',85),(1,'2016-01-13 00:00:00',50),(1,'2016-01-13 03:00:00',15),(1,'2016-01-13 06:00:00',0),(1,'2016-01-13 09:00:00',15),(1,'2016-01-13 12:00:00',50),(1,'2016-01-13 15:00:00',85),(1,'2016-01-13 18:00:00',100),(1,'2016-01-13 21:00:00',85),(1,'2016-01-14 00:00:00',50),(1,'2016-01-14 03:00:00',15),(1,'2016-01-14 06:00:00',0),(1,'2016-01-14 09:00:00',15),(1,'2016-01-14 12:00:00',50),(1,'2016-01-14 15:00:00',85),(1,'2016-01-14 18:00:00',100),(1,'2016-01-14 21:00:00',85),(1,'2016-01-15 00:00:00',50),(1,'2016-01-15 03:00:00',15),(1,'2016-01-15 06:00:00',0),(1,'2016-01-15 09:00:00',15),(1,'2016-01-15 12:00:00',50),(1,'2016-01-15 15:00:00',85),(1,'2016-01-15 18:00:00',100),(1,'2016-01-15 21:00:00',85),(1,'2016-01-16 00:00:00',50),(1,'2016-01-16 03:00:00',15),(1,'2016-01-16 06:00:00',0),(1,'2016-01-16 09:00:00',15),(1,'2016-01-16 12:00:00',50),(1,'2016-01-16 15:00:00',85),(1,'2016-01-16 18:00:00',100),(1,'2016-01-16 21:00:00',85),(1,'2016-01-17 00:00:00',50),(1,'2016-01-17 03:00:00',15),(1,'2016-01-17 06:00:00',0),(1,'2016-01-17 09:00:00',15),(1,'2016-01-17 12:00:00',50),(1,'2016-01-17 15:00:00',85),(1,'2016-01-17 18:00:00',100),(1,'2016-01-17 21:00:00',85),(1,'2016-01-18 00:00:00',50),(1,'2016-01-18 03:00:00',15),(1,'2016-01-18 06:00:00',0),(1,'2016-01-18 09:00:00',15),(1,'2016-01-18 12:00:00',50),(1,'2016-01-18 15:00:00',85),(1,'2016-01-18 18:00:00',100),(1,'2016-01-18 21:00:00',85),(1,'2016-01-19 00:00:00',50),(1,'2016-01-19 03:00:00',15),(1,'2016-01-19 06:00:00',0),(1,'2016-01-19 09:00:00',15),(1,'2016-01-19 12:00:00',50),(1,'2016-01-19 15:00:00',85),(1,'2016-01-19 18:00:00',100),(1,'2016-01-19 21:00:00',85),(1,'2016-01-20 00:00:00',50),(1,'2016-01-20 03:00:00',15),(1,'2016-01-20 06:00:00',0),(1,'2016-01-20 09:00:00',15),(1,'2016-01-20 12:00:00',50),(1,'2016-01-20 15:00:00',85),(1,'2016-01-20 18:00:00',100),(1,'2016-01-20 21:00:00',85),(1,'2016-01-21 00:00:00',50),(1,'2016-01-21 03:00:00',15),(1,'2016-01-21 06:00:00',0),(1,'2016-01-21 09:00:00',15),(1,'2016-01-21 12:00:00',50),(1,'2016-01-21 15:00:00',85),(1,'2016-01-21 18:00:00',100),(1,'2016-01-21 21:00:00',85);
/*!40000 ALTER TABLE `consumption` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `customers`
--

DROP TABLE IF EXISTS `customers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `customers` (
  `customer_id` int(11) NOT NULL,
  `customer_name` varchar(45) NOT NULL,
  PRIMARY KEY (`customer_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `customers`
--

LOCK TABLES `customers` WRITE;
/*!40000 ALTER TABLE `customers` DISABLE KEYS */;
INSERT INTO `customers` VALUES (1,'mops');
/*!40000 ALTER TABLE `customers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dispositions`
--

DROP TABLE IF EXISTS `dispositions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dispositions` (
  `generator_id` int(11) NOT NULL,
  `city_id` int(11) NOT NULL,
  PRIMARY KEY (`generator_id`),
  KEY `city_id_desp_idx` (`city_id`),
  KEY `place_id_idx` (`city_id`),
  CONSTRAINT `city_id_dispositions` FOREIGN KEY (`city_id`) REFERENCES `locations` (`location_id`) ON UPDATE CASCADE,
  CONSTRAINT `generator_id_dispositions` FOREIGN KEY (`generator_id`) REFERENCES `generators` (`generator_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dispositions`
--

LOCK TABLES `dispositions` WRITE;
/*!40000 ALTER TABLE `dispositions` DISABLE KEYS */;
/*!40000 ALTER TABLE `dispositions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `forecasts`
--

DROP TABLE IF EXISTS `forecasts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `forecasts` (
  `forecast_id` int(11) NOT NULL AUTO_INCREMENT,
  `location_id` int(11) NOT NULL,
  `date` datetime NOT NULL,
  `pressure` double NOT NULL,
  `temperature` double NOT NULL,
  `humidity` int(11) NOT NULL,
  `wind` double NOT NULL,
  PRIMARY KEY (`forecast_id`),
  UNIQUE KEY `city_id_date_UNIQUE` (`location_id`,`date`),
  KEY `city_id_idx` (`location_id`),
  CONSTRAINT `city_id_forecasts` FOREIGN KEY (`location_id`) REFERENCES `locations` (`location_id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=147 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `forecasts`
--

LOCK TABLES `forecasts` WRITE;
/*!40000 ALTER TABLE `forecasts` DISABLE KEYS */;
INSERT INTO `forecasts` VALUES (111,6,'2016-03-30 00:00:00',1023.1,-2.49,81,2.31),(112,6,'2016-03-30 03:00:00',1023.59,-3.73,80,1.26),(113,6,'2016-03-30 06:00:00',1023.72,2.4,100,1.96),(114,6,'2016-03-30 09:00:00',1023.09,3.76,100,2.07),(115,6,'2016-03-30 12:00:00',1022.69,3.64,99,1.96),(116,6,'2016-03-30 15:00:00',1022.34,2.35,97,1.62),(117,6,'2016-03-30 18:00:00',1022.15,2.15,98,1.93),(118,6,'2016-03-30 21:00:00',1021.72,1.71,97,1.51),(119,6,'2016-03-31 00:00:00',1021.45,1.26,100,1.61),(120,6,'2016-03-31 03:00:00',1021.36,1.01,100,1.81),(121,6,'2016-03-31 06:00:00',1021.18,1.61,100,1.87),(122,6,'2016-03-31 09:00:00',1020.26,2.06,99,0.26),(123,6,'2016-03-31 12:00:00',1019,2,98,1.42),(124,6,'2016-03-31 15:00:00',1018.09,0.74,99,1.17),(125,6,'2016-03-31 18:00:00',1017.04,0.43,94,1.31),(126,6,'2016-03-31 21:00:00',1015.47,-0.28,96,1.5),(127,6,'2016-04-01 00:00:00',1013.98,0.13,100,1.66),(128,6,'2016-04-01 03:00:00',1012.35,-0.01,100,1.86),(129,6,'2016-04-01 06:00:00',1010.42,0.65,100,2.17),(130,6,'2016-04-01 09:00:00',1008.01,1.5,99,2.57),(131,6,'2016-04-01 12:00:00',1005.74,1.9,92,3.22),(132,6,'2016-04-01 15:00:00',1004.57,1.34,93,2.71),(133,6,'2016-04-01 18:00:00',1004.66,1.1,92,3.41),(134,6,'2016-04-01 21:00:00',1004.22,0.48,94,1.42),(135,6,'2016-04-02 00:00:00',1003.38,0.11,96,1.36),(136,6,'2016-04-02 03:00:00',1002.33,0.39,97,1.42),(137,6,'2016-04-02 06:00:00',999.84,1.15,96,3.7),(138,6,'2016-04-02 09:00:00',996.2,1.6,93,5.56),(139,6,'2016-04-02 12:00:00',991.64,2.46,91,5.75),(140,6,'2016-04-02 15:00:00',989.39,3.36,87,6.21),(141,6,'2016-04-02 18:00:00',990.64,2.22,88,6.11),(142,6,'2016-04-02 21:00:00',989.87,1.26,92,6.57),(143,6,'2016-04-03 00:00:00',988.23,0.91,94,7.36),(144,6,'2016-04-03 03:00:00',987.35,0.82,97,6.86),(145,6,'2016-04-03 06:00:00',990.13,0.87,97,6.62),(146,6,'2016-04-03 09:00:00',993.26,1.21,98,5.87);
/*!40000 ALTER TABLE `forecasts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `generators`
--

DROP TABLE IF EXISTS `generators`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `generators` (
  `generator_id` int(11) NOT NULL AUTO_INCREMENT,
  `model_id` int(11) NOT NULL,
  PRIMARY KEY (`generator_id`),
  KEY `model_id_generators_idx` (`model_id`),
  CONSTRAINT `model_id_generators` FOREIGN KEY (`model_id`) REFERENCES `models` (`model_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `generators`
--

LOCK TABLES `generators` WRITE;
/*!40000 ALTER TABLE `generators` DISABLE KEYS */;
INSERT INTO `generators` VALUES (13,9),(14,10),(15,11),(16,12),(17,14),(18,15),(19,16);
/*!40000 ALTER TABLE `generators` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `locations`
--

DROP TABLE IF EXISTS `locations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `locations` (
  `location_id` int(11) NOT NULL AUTO_INCREMENT,
  `city_name` varchar(45) NOT NULL,
  `country_name` varchar(45) NOT NULL,
  PRIMARY KEY (`location_id`),
  UNIQUE KEY `city_name_country_UNIQUE` (`city_name`,`country_name`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `locations`
--

LOCK TABLES `locations` WRITE;
/*!40000 ALTER TABLE `locations` DISABLE KEYS */;
INSERT INTO `locations` VALUES (1,'Moscow','ru'),(6,'Samara','ru');
/*!40000 ALTER TABLE `locations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `manufacturers`
--

DROP TABLE IF EXISTS `manufacturers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `manufacturers` (
  `manufacturer_id` int(11) NOT NULL AUTO_INCREMENT,
  `manufacturer_name` varchar(45) NOT NULL,
  PRIMARY KEY (`manufacturer_id`),
  UNIQUE KEY `manufacturer_name_UNIQUE` (`manufacturer_name`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `manufacturers`
--

LOCK TABLES `manufacturers` WRITE;
/*!40000 ALTER TABLE `manufacturers` DISABLE KEYS */;
INSERT INTO `manufacturers` VALUES (6,'Endurance'),(7,'Gamesa'),(8,'Vestas');
/*!40000 ALTER TABLE `manufacturers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `models`
--

DROP TABLE IF EXISTS `models`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `models` (
  `model_id` int(11) NOT NULL AUTO_INCREMENT,
  `manufacturer_id` int(11) NOT NULL,
  `model_name` varchar(45) NOT NULL,
  `power_nom` int(11) NOT NULL,
  `cut_in` double NOT NULL,
  `cut_nom` double NOT NULL,
  `cut_out` double NOT NULL,
  `diameter` double NOT NULL,
  `number_of_blades` int(11) NOT NULL,
  PRIMARY KEY (`model_id`),
  UNIQUE KEY `manufacturer_id_mode_name_UNIQUE` (`manufacturer_id`,`model_name`),
  KEY `manufacturer_id_models_idx` (`manufacturer_id`),
  CONSTRAINT `manufacturer_id_models` FOREIGN KEY (`manufacturer_id`) REFERENCES `manufacturers` (`manufacturer_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `models`
--

LOCK TABLES `models` WRITE;
/*!40000 ALTER TABLE `models` DISABLE KEYS */;
INSERT INTO `models` VALUES (9,6,'E3120-4',65,4,14,25,19.2,3),(10,7,'G80-2.0 MW',2000,4,15,25,80,3),(11,7,'G87-2.0 MW',2000,4,14,25,87,3),(12,7,'G90-2.0 MW',2000,3,13,21,90,3),(14,7,'G97-2.0 MW',2000,3,12,25,97,3),(15,7,'G114-2.0 MW',2000,3,11,25,114,3),(16,8,'V82-1.65 MW',1650,3.5,13,24,82,3);
/*!40000 ALTER TABLE `models` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `power_output`
--

DROP TABLE IF EXISTS `power_output`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `power_output` (
  `generator_id` int(11) NOT NULL,
  `forecast_id` int(11) NOT NULL,
  `power_output` int(11) NOT NULL,
  PRIMARY KEY (`generator_id`,`forecast_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `power_output`
--

LOCK TABLES `power_output` WRITE;
/*!40000 ALTER TABLE `power_output` DISABLE KEYS */;
/*!40000 ALTER TABLE `power_output` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-04-01  2:57:58
